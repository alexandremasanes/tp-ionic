import {Trivia} from "./trivia";

export interface Trivias {
  response_code: number
  results: Trivia[]
}