export enum Type {
  BOOLEAN = 'Boolean',
  MULTIPLE = 'Multiple',
}

export enum Difficulty {
  EASY = 'Easy',
  MEDIUM = 'Medium',
  HARD = 'Hard',
}

export interface Trivia {
  category: string
  type: Type
  difficulty: Difficulty
  question: string
  correct_answer: string
  incorrect_answers: string[]
}