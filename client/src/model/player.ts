export interface Player {
  nickname: string
  score: number
  time: number
  avatar_url: string
}