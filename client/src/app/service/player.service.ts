import {EventEmitter, Injectable} from '@angular/core';
import {Service} from "./service";
import {environment} from "../../environments/environment";
import { map } from 'rxjs/operators';
import {Observable} from "rxjs/internal/Observable";
import {Player} from "../../model/player";

const API_URI: string = environment.playerApiURI;

@Injectable()
export class PlayerService extends Service {

  public readonly gameCompleted: EventEmitter<{score: number, time: number}>
    = new EventEmitter;

  getAll(): Observable<Player[]> {
    return this.httpClient.get<Player[]>(`${API_URI}/json`);
  }

  getOne(nickname: string): Observable<Player> {
    return this.httpClient.get<Player[]>(`${API_URI}/json/${nickname}`)
                          .pipe<Player>(
                            map(
                              (players, _) =>
                                players.sort(
                                  (p0, p1) =>
                                    (p0.score - p1.score) || p1.time - p0.time
                                )[0]
                            )
                          );
  }

  create(player: Player): Observable<void> {
    return this.httpClient.post<void>(`${API_URI}/score`, player);
  }
}
