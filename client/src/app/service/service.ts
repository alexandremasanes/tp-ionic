import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export abstract class Service {

  public constructor(protected readonly httpClient: HttpClient) {}
}