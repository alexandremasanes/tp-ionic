import { Injectable } from '@angular/core';

import {environment} from "../../environments/environment";
import {Observable} from "rxjs/internal/Observable";
import {Trivias} from "../../model/trivias";
import {Service} from "./service";
import {Difficulty} from "../../model/trivia";

const API_URI: string = environment.triviaApiURI;

@Injectable()
export class TriviaService extends Service {

  get(amount: number, diffulty: Difficulty): Observable<Trivias> {
    return this.httpClient.get<Trivias>(
      API_URI,
      {
        params: {
          amount: amount.toString(),
          difficulty: diffulty[diffulty],
        }
      }
    );
  }
}
