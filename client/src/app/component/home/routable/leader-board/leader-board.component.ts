import { Component, OnInit } from '@angular/core';
import {Player} from "../../../../../model/player";
import {PlayerService} from "../../../../service/player.service";

@Component({
  selector: 'app-leader-board',
  templateUrl: './leader-board.component.html',
  styleUrls: ['./leader-board.component.scss']
})
export class LeaderBoardComponent implements OnInit {

  players: Player[];

  constructor(private readonly playerService: PlayerService) {
    this.playerService.getAll()
                      .subscribe(players => {
                        let nicknames: string[];

                        players = players.sort(
                          (p0, p1) =>
                            (p1.score - p0.score) || p0.time - p1.time
                        );

                        nicknames = players.map(
                          player => player.nickname
                        );

                        this.players = players.filter(
                          (player, index) =>
                            index == nicknames.lastIndexOf(player.nickname)
                        )
                      });
  }

  ngOnInit() {
  }

}
