import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Player} from "../../../model/player";
import {PlayerService} from "../../service/player.service";

const URL_PATTERN: string = "https?:\\/\\/(www\\.)?[-a-zA-Z0-9@:%._\\+~#=]{2,256}" +
                           "\\.[a-z]{2,6}\\b([-a-zA-Z0-9@:%_\\+.~#?&//=]*)";

@Component({
  selector: 'app-player-signup',
  templateUrl: './player-signup.component.html',
  styleUrls: ['./player-signup.component.scss']
})
export class PlayerSignupComponent implements OnInit {

  player: Player;

  @Output("playerCreated")
  eventEmitter: EventEmitter<Player>;

  alreadyRegistered: boolean;

  constructor(private readonly playerService: PlayerService) {
    this.eventEmitter = new EventEmitter;

    this.player = {
      nickname: null,
      avatar_url: null,
      score: null,
      time: null,
    };
  }

  retrievePlayer() {
    this.playerService.getOne(this.player.nickname)
                      .subscribe(player => {
                        if(player != null)
                          this.eventEmitter.emit(player);
                        else
                          this.alreadyRegistered = false;
                      });
  }

  registerPlayer() {
    this.playerService.create(this.player)
                      .subscribe(() =>
                        this.eventEmitter.emit(this.player)
                      );
  }

  get completed(): boolean {
    return this.player.nickname != null &&
      this.alreadyRegistered ||
      !this.player.avatar_url != null;
  }

  get urlPattern(): string {
    return URL_PATTERN;
  }

  ngOnInit() {
  }

}
